# Debianópolis em festa

No dia 14 de agosto de 2021, o Projeto Debian lançará a nova versão estável da sua distribuição GNU/Linux, o Debian Bullseye. Na sequência, no dia 16 de agosto, também acontece o tradicional evento comemorativo do aniversário do projeto: o DebianDay.

Observando a proximidade das datas, que vão de um sábado a uma segunda-feira, as comunidades do Curso GNU (do professor Kretcheu) e debxp decidiram fazer três dias seguidos de festa com várias atividades em torno dos dois eventos.

## O Debian é Livre!

O tema proposto como linha guia para as palestras e discussões que acontecerão durante os festejos será: **o Debian é Livre**, com o qual pretendemos refletir sobre o que é a liberdade de computação, o que são as diretrizes para distribuição de sistemas livres (FSDG), da Free Software Foudation, as diretrizes Debian sobre Software Livre (DFSG) e o que é Software Livre.

## Por que "Debianópolis"?

A rigor, o registro desse tipo de celebração na wiki do Projeto Debian é feito em nome de uma cidade, mas nós acreditamos que este paradigma morreu para sempre.

Hoje, as comunidades de utilizadores, entusiastas e desenvolvedores atuam muito mais no suporte e trazendo novos utilizadores através de suas mídias sociais e grupos em mensageiros instantâneos, o que transcende os limites de municípios e até nacionalidades! Some a isso a particularidade que vivemos atualmente com a pandemia e você verá que não faria o menor sentido nós registrarmos o evento em nome de uma única cidade:

* [Registro de cidades para o DebianDay](https://wiki.debian.org/DebianDay/2021)

* [Registro de cidades para o Debian ReleaseParty](https://wiki.debian.org/ReleasePartyBullseye)

Portanto, em nome desta nova realidade cosmopolitana, nós decidimos criar uma cidade brasileira virtual, sem fronteiras de estados, chamada "Debianópolis". Assim, onde quer que você esteja no mundo, você é um cidadão de Debianópolis, Brasil!

## Atividades

Para comemorar os dois eventos, nós programamos três dias de atividades envolvendo:

* Palestras online com convidados;
* Palestras relâmpago com a comunidade;
* Parlatório;
* Festival de atualização e instalação;
* Laboratório de conversões e reparos.

## Programação

**Todos os horários são UTC-3!**

### Sábado, 14 de agosto de 2021

| Tema/Atividade   | Horário | Palestrantes    | Descrição        |
|-------------|---------|-----------------|-----------------------|
| [**Abertura**](https://youtu.be/zCYJs5M6tFg)    | 14:00   | Blau e Kretcheu | Abertura dos festejos |
| [**Territórios Digitais Livres**](https://youtu.be/KBoDxEmG6c0) | 14:30   | Casa de Cultura Tainã | **Recompilando o kernel da alma.**<br/>Venha conhecer os objetivos e as práticas da Rota dos Baobás, que busca identificar e mapear saberes tradicionais comunitários e desenvolver ações colaborativas para o desenvolvimento de tecnologias sociais que contribuam com a promoção do desenvolvimento local das comunidades envolvidas na Rede Mocambos. |
| [**Debian Package Tracker**](https://youtu.be/zPjtiU6VeVQ) | 15:30   | João Eriberto Mota Filho | Todo usuário deveria conhecer o Tracker. |
| [**FAQ Debian**](https://youtu.be/P_aJodFUidg) | 16:30   | Paulo Kretcheu  | Desmistificando o Debian           |
| [**O professor de Debianópolis**](https://youtu.be/QYgveO7KUUM) | 17:30   | Ramon Mulin     | **Trabalhando com softwares livres nas escolas.**<br/>O objetivo desta palestra/laboratório é demonstrar na prática o trabalho real de um professor apenas com softwares livres. |
| [**O Debian é Livre!**](https://youtu.be/TTDBvUg5oKU) | 18:30   | Blau Araujo     | Vamos refletir juntos sobre o que é a liberdade de computação, o que são as diretrizes para distribuição de sistemas livres (FSDG), da Free Software Foudation, as diretrizes Debian sobre Software Livre (DFSG) e o que é Software Livre. |
| [**Intercooperativas**](https://youtu.be/CHkE4DrWKuM) | 19:30 | Lívia Gouvea | **Cooperativismo em rede**<br/>Reflexões sobre como fomentar a criação de novas cooperativas com a ideia de formar uma federação capaz de fortalecer a rede através da intercooperacão. |
| [**Palestras Relâmpago**](https://youtu.be/BMPBd7wtPsA) | 20:30   | Membros da comunidade | O link do Jitsi será divulgado ao final da palestra anterior para que todos que desejarem participem. Cada participante inscrito falará por 5 minutos sobre o tema que quiser, desde que seja em torno do Debian. |

* **As transmissões ao vivo também poderão ser assistidas pelo seu player livre favorito ou pelo site da comunidade debxp:** https://debxp.org/lives
* Para interagir conosco durante as transmissões, utilize o chat do Youtube ou, se preferir, entre na sala #debianopolis2021 no servidor IRC Libera Chat.
* Acesso web ao IRC: https://web.libera.chat/#debianopolis2021

### Domingo, 15 de agosto de 2021

**Este evento não será transmitido!**

A partir das 14h, será aberta uma sala de triagem no Jitsi para suporte e acompanhamento de atualizações para a versão estável do Debian, novas instalações e até de migrações para quem vem de outras distribuições.

Dependendo do tipo de intervenção necessária e da quantidade de participantes, novas salas serão abertas para um acompanhamento mais adequado.

Em paralelo, será divulgada uma sala no IRC que concentrará e coordenará a comunicação entre todas as salas abertas.

### Segunda, 16 de agosto de 2021

Às 20h, na [Live de Segunda](https://debxp.org/lives), nós faremos um parlatório para discutirmos o Projeto Debian, a comunidade, o lançamento do Bullseye, o que esperar das versões anteriores e posteriores, os resultados do fim de semana e, principalmente, o tema proposto para esta celebração:

**O Debian é livre!**

## Divulgação

[![](https://debxp.org/site/wp-content/uploads/2021/08/debianopolis-2021-212x300.png)](https://debxp.org/site/wp-content/uploads/2021/08/debianopolis-2021.png)
